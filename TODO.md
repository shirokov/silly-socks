## Roadmap:
- 0.3.2 - add functional tests
- 0.4.0 - go ansyc
- 0.5.0 - BIND support
- 0.6.0 - UDP support
- 0.7.0 - IPv6 support
- 1.0.0 - GSSAPI authentication

## Other stuff to do:
- address pylint issues
- flexible timeout format in the config, like "500ms", "1m10s", or "1d2h"
- more unit tests!
- functional tests
- get rid of the legacy tests
- current coroutines implementation contains a lot of duplicated code and is horribly long
- switch to Python 3.5+ async await coroutines syntax: generator-style coroutines will be removed in Python 3.10
- switch from selectors to asyncio
- make socket buffer sizes configurable + "max" setting
- transfer buffer is "jerky" -- pipe starts reading again only after the whole buffer if flushed
- validate YAML config: error on unexpected elements, warn about inconsistent settings, etc
- log file should be rotatable
- save the list of banned source addresses to a file
- file io and writing to log files on a dedicated thread
- improve white/black lists: support configuring networks, ports and port ranges, regex for hostnames
