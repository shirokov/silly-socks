# Silly SOCKS Changelog

## [0.3.1] - 2023-11-07
### Added
- Unit tests with `pytest`.
- Fixed a bug in timer queue.
### Changed
- Moved old test system into `test/legacy`.

## [0.3.0] - 2021-09-01
### Changed
- Improved logging in daemon mode
- Main executable is `src/silly-socks.py` instead of `bin/silly-socks`

### Removed
- Removed `bin/` directory

## [0.2.4] - 2020-04-26
### Added
- Configurable log file
- Running as a daemon

### Changed
- Reply with auth failure on empty username/password instead of closing connection
- Fixed unsuccessful session attempts counting
- Switched to Python's selectors module
- Fixed not sending reply on failed connect attempts
- Reduced technical debt

### Removed
- Removed 'proxy' section from the configuration

## [0.2.3] - 2019-11-10
### Added
- Configurable transfer buffer
- Rulesets that can be assigned per user
- Session and relay timeouts
- Ban source addresses after certain number of failed session attempts
- White/black address list

### Changed
- Explicitly disabled IPv6 support in requests
- Fixed logging the invalid password on unseccessful authorization attempts

### Removed
- Poll timeout configuration parameter

## [0.2.2] - 2019-02-02
### Added
- Support YAML configuration files
- Start script bin/silly-socks
- Stop gracefully on SIGTERM

### Changed
- Properly handling connect timeout

## [0.2.1] - 2019-01-28
### Added
- Username/Password authentication support

### Changed
- Various fixes

## [0.2.0] - 2019-01-17
### Added
- Configurable maximum number of clients
- CONNECT command implementation

## [0.1.2] - 2019-01-08
### Added
- Server replies with "Command not suported" on any commmand

## [0.1.1] - 2019-01-07
### Added
- Server closes the connection after receiving the Request message

### Changed
- Client session & parse messages using coroutines

## [0.1.0] - 2019-01-05
### Added
- Minimal server implementation that rejects any methods

## [0.0.0] - 2018-12-27
### Added
- Created the project
