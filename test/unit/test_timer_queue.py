#!/usr/bin/env python3

# Copyright (c) 2022 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

import timer_queue

import pytest


class CallSink:
    def __init__(self):
        self._called = 0

    def call(self):
        self._called += 1

    def count(self):
        return self._called


def test_one_timer():
    tq = timer_queue.TimerQueue()
    sink = CallSink()

    tq.new_timer(5, lambda: sink.call())
    assert tq.nearest_timer_expiry() == 5
    assert sink.count() == 0

    tq.expire_timers(2)
    assert tq.nearest_timer_expiry() == 3
    assert sink.count() == 0

    tq.expire_timers(3)
    assert tq.nearest_timer_expiry() is None
    assert sink.count() == 1

def test_disable_timer():
    tq = timer_queue.TimerQueue()
    sink = CallSink()

    t = tq.new_timer(1, lambda: sink.call())
    assert tq.nearest_timer_expiry() == 1
    assert sink.count() == 0

    t.disarm()

    tq.expire_timers(2)
    assert tq.nearest_timer_expiry() is None
    assert sink.count() == 0

def test_timer_added_during_expiry_loop():
    class MySink(CallSink):
        def __init__(self, tq, timeout):
            super().__init__()
            self._tq = tq
            self._to = timeout

        def _call_base(self):
            super().call()

        def call(self):
            self._tq.new_timer(self._to, lambda: self._call_base)
            self._call_base()

    tq = timer_queue.TimerQueue()
    sink = MySink(tq, 1)

    t = tq.new_timer(1, lambda: sink.call())
    assert tq.nearest_timer_expiry() == 1
    assert sink.count() == 0

    tq.expire_timers(2)
    assert tq.nearest_timer_expiry() == 1
    assert sink.count() == 1

def test_three_timers():
    tq = timer_queue.TimerQueue()
    sink = CallSink()

    tq.new_timer(2, lambda: sink.call())
    tq.new_timer(3, lambda: sink.call())
    tq.new_timer(1, lambda: sink.call())

    assert tq.nearest_timer_expiry() == 1
    assert sink.count() == 0

    tq.expire_timers(1)
    assert tq.nearest_timer_expiry() == 1
    assert sink.count() == 1

    tq.expire_timers(2)
    assert tq.nearest_timer_expiry() is None
    assert sink.count() == 3
