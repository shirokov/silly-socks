#!/usr/bin/env python3

# Copyright (c) 2022 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

import ruleset
import yaml_config

import os
import pytest


def test_verbosity():
    # valid values
    assert yaml_config.translate_verbosity("error") == 0
    assert yaml_config.translate_verbosity("warning") == 1
    assert yaml_config.translate_verbosity("info") == 2

    # invalid values
    with pytest.raises(ValueError, match=r"bad verbosity value: .*"):
        yaml_config.translate_verbosity("debug")

    with pytest.raises(ValueError, match=r"bad verbosity value: .*"):
        yaml_config.translate_verbosity("Error")


def test_invalid_path():
    with pytest.raises(Exception):
        yaml_config.parse(
            "/dev/null/config.yml"
        )  # the path is invalid on POSIX systems


def test_no_such_file(tmp_path):
    fname = tmp_path / "config.yml"
    assert not os.path.isfile(fname)
    with pytest.raises(Exception):
        yaml_config.parse(fname)


def test_empty_file(tmp_path):
    fname = tmp_path / "config.yml"
    fname.write_text("")
    with pytest.raises(RuntimeError, match=r"invalid configuration file"):
        yaml_config.parse(fname)


def test_no_actual_config(tmp_path):
    fname = tmp_path / "config.yml"
    fname.write_text("# just a comment")
    with pytest.raises(RuntimeError, match=r"invalid configuration file"):
        yaml_config.parse(fname)


def test_server_config(tmp_path):
    fname = tmp_path / "config.yml"
    fname.write_text(
        (
            "server:\n"
            "   host: 1.2.3.4\n"
            "   port: 42\n"
            "   max-clients: 10\n"
            "   transfer-buffer: 4096\n"
            "   session-timeout: 111\n"
            "   max-session-attempts: 222\n"
            "   listener-backlog: 333\n"
            "   daemon: yes\n"
            "   pid-file: test.pid\n"
            "   verbosity: info\n"
            "   log-file: test.log\n"
            "   debug-mode: yes\n"
        )
    )
    config = yaml_config.parse(fname)

    assert config.host == "1.2.3.4"
    assert config.port == 42
    assert config.users == {}
    assert config.no_auth == False
    assert config.listener_backlog == 333
    assert config.max_clients == 10
    assert config.transfer_buffer == 4096
    assert config.session_timeout == 111
    assert config.max_session_attempts == 222
    assert config.daemon == True
    assert config.pid_file == "test.pid"
    assert config.verbosity == 2
    assert config.log_file == "test.log"
    assert config.debug_mode == True
    assert config.default_ruleset.name == "default"
    assert config.default_ruleset.relay_timeout == 0
    assert config.default_ruleset.connect_enabled == True
    assert config.default_ruleset._addr_list is None
    assert config.default_ruleset._mode == ruleset.Ruleset._Modes.UNRESTRICTED
    assert config.rulesets == {}
    assert config.rules == {}


def test_auth_config(tmp_path):
    fname = tmp_path / "config.yml"
    fname.write_text(
        (
            "auth:\n"
            "   no-auth: yes\n"
            "   users:\n"
            "       - alice: bar\n"
            "       - bob: baz\n"
        )
    )
    config = yaml_config.parse(fname)

    assert config.host is None
    assert config.port is None
    assert config.users == {"alice": "bar", "bob": "baz"}
    assert config.no_auth == True
    assert config.listener_backlog == 32
    assert config.max_clients == 0
    assert config.transfer_buffer == 0
    assert config.session_timeout == 0
    assert config.max_session_attempts == 0
    assert config.daemon == False
    assert config.pid_file == "/var/run/silly-socks.pid"
    assert config.verbosity == 0
    assert config.log_file == None
    assert config.debug_mode == False
    assert config.default_ruleset.name == "default"
    assert config.default_ruleset.relay_timeout == 0
    assert config.default_ruleset.connect_enabled == True
    assert config.default_ruleset._addr_list is None
    assert config.default_ruleset._mode == ruleset.Ruleset._Modes.UNRESTRICTED
    assert config.rulesets == {}
    assert config.rules == {}


def test_rulesets_config(tmp_path):
    fname = tmp_path / "config.yml"
    fname.write_text(
        (
            "rulesets:\n"
            "   default:\n"
            "       relay-timeout: 13\n"
            "       connect-enabled: no\n"
            "       black-list:\n"
            "           - foo\n"
            "           - bar\n"
            "   custom:\n"
            "       relay-timeout: 42\n"
            "       connect-enabled: yes\n"
            "       white-list:\n"
            "           - baz\n"
            "           - bar\n"
        )
    )
    config = yaml_config.parse(fname)

    assert config.host is None
    assert config.port is None
    assert config.users == {}
    assert config.no_auth == False
    assert config.listener_backlog == 32
    assert config.max_clients == 0
    assert config.transfer_buffer == 0
    assert config.session_timeout == 0
    assert config.max_session_attempts == 0
    assert config.daemon == False
    assert config.pid_file == "/var/run/silly-socks.pid"
    assert config.verbosity == 0
    assert config.log_file == None
    assert config.debug_mode == False
    assert config.default_ruleset.name == "default"
    assert config.default_ruleset.relay_timeout == 13
    assert config.default_ruleset.connect_enabled == False
    assert config.default_ruleset._addr_list == ["foo", "bar"]
    assert config.default_ruleset._mode == ruleset.Ruleset._Modes.BLACK_LIST
    assert len(config.rulesets) == 1
    assert "custom" in config.rulesets
    assert config.rulesets["custom"].name == "custom"
    assert config.rulesets["custom"].relay_timeout == 42
    assert config.rulesets["custom"].connect_enabled == True
    assert config.rulesets["custom"]._addr_list == ["baz", "bar"]
    assert config.rulesets["custom"]._mode == ruleset.Ruleset._Modes.WHITE_LIST
    assert config.rules == {}


def test_rulesets_config_mode_conflict(tmp_path):
    fname = tmp_path / "config.yml"
    fname.write_text(
        (
            "rulesets:\n"
            "   custom:\n"
            "       white-list:\n"
            "           - foo\n"
            "       black-list:\n"
            "           - bar\n"
        )
    )
    with pytest.raises(RuntimeError, match=r"both black and white lists are specified"):
        yaml_config.parse(fname)


def test_rules_config(tmp_path):
    fname = tmp_path / "config.yml"
    fname.write_text(
        (
            "# we need to configure a user for rules to work...\n"
            "auth:\n"
            "   users:\n"
            "       - user: password\n"
            "# ...and a ruleset to assign to this user\n"
            "rulesets:\n"
            "   ruleset:\n"
            "       relay-timeout: 314\n"
            "rules:\n"
            "   user: ruleset\n"
        )
    )
    config = yaml_config.parse(fname)

    assert config.host is None
    assert config.port is None
    assert "user" in config.users
    assert config.no_auth == False
    assert config.listener_backlog == 32
    assert config.max_clients == 0
    assert config.transfer_buffer == 0
    assert config.session_timeout == 0
    assert config.max_session_attempts == 0
    assert config.daemon == False
    assert config.pid_file == "/var/run/silly-socks.pid"
    assert config.verbosity == 0
    assert config.log_file == None
    assert config.debug_mode == False
    assert config.default_ruleset.name == "default"
    assert config.default_ruleset.relay_timeout == 0
    assert config.default_ruleset.connect_enabled == True
    assert config.default_ruleset._addr_list is None
    assert config.default_ruleset._mode == ruleset.Ruleset._Modes.UNRESTRICTED
    assert len(config.rulesets) == 1
    assert "ruleset" in config.rulesets
    assert len(config.rules) == 1
    assert "user" in config.rules
    assert config.rules["user"] is config.rulesets["ruleset"]


def test_rules_config_no_user(tmp_path):
    fname = tmp_path / "config.yml"
    fname.write_text(
        (
            "rulesets:\n"
            "   ruleset:\n"
            "       relay-timeout: 314\n"
            "rules:\n"
            "   user: ruleset\n"
        )
    )
    with pytest.raises(RuntimeError, match=r"rule for unknown user 'user'"):
        yaml_config.parse(fname)


def test_rules_config_no_ruleset(tmp_path):
    fname = tmp_path / "config.yml"
    fname.write_text(
        (
            "auth:\n"
            "   users:\n"
            "       - user: password\n"
            "rules:\n"
            "   user: ruleset\n"
        )
    )
    with pytest.raises(
        RuntimeError, match=r"rule for user 'user' refers to unknown ruleset 'ruleset'"
    ):
        yaml_config.parse(fname)
