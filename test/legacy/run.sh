#!/bin/sh

# Copyright (c) 2018-2022 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

export TEST_HOST=localhost
export TEST_PORT=1080
export TEST_SINK_HOST=localhost
export TEST_SINK_PORT=5001
export TEST_USER=client1
export TEST_PASSWORD=test

rm -f *.log

LOG_FILE=`mktemp /tmp/silly-socks.log.XXXXXX`
PID_FILE=`mktemp /tmp/silly-socks.pid.XXXXXX`

../../src/silly-socks.py --no-auth -l $LOG_FILE -P $PID_FILE -f test.yml $TEST_HOST $TEST_PORT
sleep 1  # give the test instance a chance to start

echo "===[ Test 1 ]==="
./test1.py > test1.log 2>&1
echo "===[ Test 2 ]==="
./test2.py > test2.log 2>&1
echo "===[ Test 3 ]==="
./test3.py > test3.log 2>&1
echo "===[ Test 4 ]==="
./test4.py > test4.log 2>&1

kill `cat $PID_FILE`
wait

mv $LOG_FILE ./server.log
rm -f $PID_FILE
