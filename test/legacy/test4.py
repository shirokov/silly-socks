#!/usr/bin/env python3

# Copyright (c) 2018-2022 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

''' Simple test for Silly SOCKS #4 '''

import ipaddress
import logging
import sys

sys.path.append("../../src")

import socks5.messages


from misc import *


def login():
    client = Client(os.environ['TEST_HOST'], int(os.environ['TEST_PORT']))
    logging.debug("Connection OK")

    client.send(socks5.messages.VersionAndMethods([socks5.constants.Methods.USERNAME_PASSWORD]))
    reply = client.recv(socks5.messages.MethodSelection)

    if reply.method == socks5.constants.Methods.NO_ACCEPTABLE_METHOD:
        logging.error("Server declined our methods")
        return None

    logging.info("Server has selected method %d", reply.method)
    if reply.method != socks5.constants.Methods.USERNAME_PASSWORD:
        raise RuntimeError(f"wrong auth method: {reply.method}")

    return client


def invalid_user_test():
    logging.info("Running invalid user test...")

    client = login()
    if client is not None:
        client.send(socks5.messages.UsernamePasswordRequest('fake', 'bogus'))

        reply = client.recv(socks5.messages.AuthenticationResponse)
        if reply.status == socks5.constants.AuthenticationStatuses.SUCCESS:
            logging.error("logged in with invalid user")

        client.close()


def zero_len_uname_test():
    logging.info("Running zero length user name test...")

    client = login()
    if client is not None:
        client.send(socks5.messages.UsernamePasswordRequest('', 'bogus'))

        reply = client.recv(socks5.messages.AuthenticationResponse)
        if reply.status == socks5.constants.AuthenticationStatuses.SUCCESS:
            logging.error("logged in with zero length user name")

        client.close()


def zero_len_passwd_test():
    logging.info("Running zero length password test...")

    client = login()
    if client is not None:
        client.send(socks5.messages.UsernamePasswordRequest('fake', ''))

        reply = client.recv(socks5.messages.AuthenticationResponse)
        if reply.status == socks5.constants.AuthenticationStatuses.SUCCESS:
            logging.error("logged in with zero length password")

        client.close()


def invalid_password_test():
    logging.info("Running invalid password test...")

    client = login()
    if client is not None:
        uname = os.environ['TEST_USER']
        passwd = 'bogus' + os.environ['TEST_PASSWORD']

        client.send(socks5.messages.UsernamePasswordRequest(uname, passwd))

        reply = client.recv(socks5.messages.AuthenticationResponse)
        if reply.status == socks5.constants.AuthenticationStatuses.SUCCESS:
            logging.error("logged in with invalid password")

        client.close()


def login_test():
    logging.info("Running valid user test...")

    client = login()
    if client is not None:
        uname = os.environ['TEST_USER']
        passwd = os.environ['TEST_PASSWORD']

        client.send(socks5.messages.UsernamePasswordRequest(uname, passwd))

        reply = client.recv(socks5.messages.AuthenticationResponse)

        if reply.status != socks5.constants.AuthenticationStatuses.SUCCESS:
            logging.error("failed to login")

        client.close()


def banned_connect(addr):
    client = login()
    if client is not None:
        uname = os.environ['TEST_USER']
        passwd = os.environ['TEST_PASSWORD']

        client.send(socks5.messages.UsernamePasswordRequest(uname, passwd))

        reply = client.recv(socks5.messages.AuthenticationResponse)

        if reply.status != socks5.constants.AuthenticationStatuses.SUCCESS:
            logging.error("failed to login")

        addr_type = socks5.constants.AddressTypes.DOMAINNAME if isinstance(addr, str) \
                        else socks5.constants.AddressTypes.IP_V4
        client.send(socks5.messages.Request(socks5.constants.Commands.CONNECT, addr_type, addr, 0))

        reply = client.recv(socks5.messages.Reply)

        client.close()

def banned_host_test():
    logging.info("Running banned host test...")
    banned_connect('banned-host')


def banned_ip_test():
    logging.info("Running banned host test...")
    banned_connect(ipaddress.ip_address('0.1.2.3'))


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s %(levelname)s:%(message)s")

    login_test()  # NOTE: login test doesn't estabilish a connection, thus this attempt counts as an unsuccesfull
    banned_host_test()
    banned_ip_test()
    invalid_password_test()
    zero_len_uname_test()
    zero_len_passwd_test()
    logging.info("Run invalid user test 4 times in order to trigger ban...")
    for i in range(4):
        logging.info("...#%d...", i + 1)
        invalid_user_test()
    logging.info("...and expect login test to fail due to ban")
    login_test()
