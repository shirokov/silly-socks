#!/usr/bin/env python3

# Copyright (c) 2018-2022 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

''' Simple test for Silly SOCKS #1 '''

import ipaddress
import logging
import os
import sys

sys.path.append("../../src")

import socks5.constants
import socks5.messages

from client import Client
from misc import *


def invalid_version_test():
    logging.info("Running invalid protocol version test...")

    client = Client(os.environ['TEST_HOST'], int(os.environ['TEST_PORT']))
    logging.info("Connected to server")

    client.send_data(bytes([0, 0]))  # version is 0 and no methods

    # FIXME: check if we got 0 bytes
    client.recv_data(1)
    client.close()


def invalid_addr_type_test():
    logging.info("Running invalid address type test...")

    client = connect_client()
    if client is not None:
        client.send(socks5.messages.Request(socks5.constants.Commands.CONNECT,
                                            0xFF,
                                            ipaddress.IPv4Address('0.0.0.0').packed,
                                            80,
                                            check_address_type=False
                                        )
                )
        # FIXME: check if we got 0 bytes
        client.recv_data(1)

    client.close()


def invalid_command_test():
    logging.info("Running invalid command test...")

    client = connect_client()
    if client is not None:
        client.send(socks5.messages.Request(0xFF,
                                            socks5.constants.AddressTypes.IP_V4,
                                            ipaddress.IPv4Address('0.0.0.0'),
                                            80,
                                            check_command=False
                                        )
                )
        # FIXME: check if we got 0 bytes
        client.recv_data(1)

    client.close()

def session_timeout_test():
    logging.info("Running session timeout test...")

    client = connect_client()
    # FIXME: timeout for recv() & check if we got 0 bytes due to timeout
    client.recv_data(1)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s %(levelname)s:%(message)s")

    invalid_version_test()
    invalid_command_test()
    invalid_addr_type_test()
    session_timeout_test()
