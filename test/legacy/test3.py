#!/usr/bin/env python3

# Copyright (c) 2018-2022 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

''' Simple test for Silly SOCKS #3 '''

import enum
import logging
import os
import select
import socket
import sys
import time

sys.path.append("../../src")

import socks5.messages

from misc import *


sink_host = os.environ['TEST_SINK_HOST']
sink_port = int(os.environ['TEST_SINK_PORT'])


def connect_to_sink():
    client = connect_client()
    if not client:
        raise RuntimeError("connect_client() failed")

    client.send(socks5.messages.Request(socks5.constants.Commands.CONNECT,
                                        socks5.constants.AddressTypes.DOMAINNAME,
                                        sink_host,
                                        sink_port
                                    )
                )

    reply = client.recv(socks5.messages.Reply)
    logging.info("server replied: %s", reply.reply_code)

    if reply.reply_code != socks5.constants.Replies.SUCCEEDED:
        raise RuntimeError("SOCKS5 CONNECT failed")

    return client


def start_sink():
    sink = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    sink.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sink.bind((sink_host, sink_port))
    sink.listen()

    return sink


class Agent:
    def _getsockopt(self, opt):
        return self._sock.getsockopt(socket.SOL_SOCKET, opt)

    class Modes(enum.Enum):
        SENDER = 0
        RECEIVER = 1
        DONE = 2

    def __init__(self, sock, payload, mode, poll):
        self._sock = sock
        self._sock.setblocking(0)
        self._payload = payload
        self._index = 0
        self._mode = mode
        self._poll = poll
        self._poll.register(self._sock.fileno(), self._poll_events())
        self._has_switched_mode = False
        self.recv_buf_size = self._getsockopt(socket.SO_RCVBUF)
        self.send_buf_size = self._getsockopt(socket.SO_SNDBUF)
        self._total = bytes()

    def _poll_events(self):
        if self._mode == Agent.Modes.SENDER:
            return select.POLLOUT
        elif self._mode == Agent.Modes.RECEIVER:
            return select.POLLIN
        elif self._mode == Agent.Modes.DONE:
            return select.POLLHUP
        else:
            raise NotImplemented(f"Agent doesn't implement mode {self._mode}")

    def act(self):
        ''' Returns true when the agent's job is done. '''
        if self._mode == Agent.Modes.SENDER:
            sent = 0
            try:
                to_send = len(self._payload) - self._index
                sent = self._sock.send(self._payload[self._index:self._index + to_send])
            except BlockingIOError:
                pass
            self._index += sent
            logging.debug("%d bytes sent: %d left from %d payload", sent, len(self._payload) - self._index, len(self._payload))

            if self._index == len(self._payload):
                if self._has_switched_mode:
                    self._mode = Agent.Modes.DONE
                else:
                    self._mode = Agent.Modes.RECEIVER
                    self._has_switched_mode = True
                self._poll.register(self._sock.fileno(), self._poll_events())
        elif self._mode == Agent.Modes.RECEIVER:
            data = self._sock.recv(self.recv_buf_size)
            if len(data) == 0:
                raise RuntimeError(f"fd {self._sock.fileno()}: server dropped the connection")

            self._total += data
            logging.debug("%d bytes received: %d total from %d payload", len(data), len(self._total), len(self._payload))

            if self._total == self._payload:
                if self._has_switched_mode:
                    self._mode = Agent.Modes.DONE
                else:
                    self._mode = Agent.Modes.SENDER
                    self._has_switched_mode = True
                self._poll.register(self._sock.fileno(), self._poll_events())
        return self._mode == Agent.Modes.DONE

    def close(self):
        self._poll.unregister(self._sock.fileno())
        self._sock.close()


# number of streams should be <= the listener backlog = 512
# as the client agents connect before the sink gets a chance
# to accept
# TODO: fix by non-blocking connect
def load_test(streams, payload):
    logging.info("Running load test for connect: %d streams, payload %dkb...", streams, len(payload) / 1024)

    poll = select.poll()
    sink = start_sink()
    poll.register(sink.fileno(), select.POLLIN)

    agents = {}  # fd -> Agent
    for i in range(streams):
        client_sock = connect_to_sink()._sock
        agents[client_sock.fileno()] = Agent(client_sock, payload, Agent.Modes.SENDER, poll)

    complete = 0
    while complete < len(agents):
        for fd, event in poll.poll():
            if fd == sink.fileno():
                sock, addr = sink.accept()
                logging.debug("creating an agent for connection from %s:%d", addr[0], addr[1])
                agents[sock.fileno()] = Agent(sock, payload, Agent.Modes.RECEIVER, poll)
            else:
                logging.debug("fd=%d event=%d", fd, event)
                if agents[fd].act():
                    complete += 1

    sink.close()
    for a in agents.values():
        a.close()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s %(levelname)s:%(message)s")

    megabyte = random_payload(2 ** 20)
    load_test(8, 4 * megabyte)
