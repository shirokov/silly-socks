#!/usr/bin/env python3

# Copyright (c) 2018-2022 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

''' Simple test for Silly SOCKS #1 '''

import ipaddress
import logging
import os
import socket
import sys

sys.path.append("../../src")

import socks5.constants
import socks5.messages

from misc import *


def do_connect(address_type, address, port):
    client = connect_client()
    if client is not None:
        client.send(socks5.messages.Request(socks5.constants.Commands.CONNECT,
                                            address_type,
                                            address,
                                            port
                                        )
                )

        reply = client.recv(socks5.messages.Reply)
        logging.info("server replied: %s", reply.reply_code)

        client.close()


def ipv6_test():
    logging.info("Running IPv6 test...")
    do_connect(socks5.constants.AddressTypes.IP_V6, ipaddress.IPv6Address('::1'), 443)


def invalid_conn_arg_test():
    logging.info("Running invalid connect argument test...")
    do_connect(socks5.constants.AddressTypes.DOMAINNAME, '0.0.200.200:443', 443)


def invalid_port_test():
    logging.info("Running invalid port test...")
    do_connect(socks5.constants.AddressTypes.DOMAINNAME, os.environ['TEST_SINK_HOST'], 0)


def closed_port_test():
    logging.info("Running closed port test...")
    do_connect(socks5.constants.AddressTypes.DOMAINNAME, os.environ['TEST_SINK_HOST'], int(os.environ['TEST_SINK_PORT']))


def invalid_dst_test():
    logging.info("Running invalid destination test...")
    do_connect(socks5.constants.AddressTypes.DOMAINNAME, '0.1.2.3', 1080)


def client_drops_before_dest_test():
    logging.info("Running client drops before destination test...")

    client = connect_client()
    if client is not None:
        sink_host = os.environ['TEST_SINK_HOST']
        sink_port = int(os.environ['TEST_SINK_PORT'])

        sink = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sink.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sink.bind((sink_host, sink_port))
        sink.listen()

        client.send(socks5.messages.Request(socks5.constants.Commands.CONNECT,
                                            socks5.constants.AddressTypes.DOMAINNAME,
                                            sink_host,
                                            sink_port
                                        )
                )

        sock, addr = sink.accept()

        reply = client.recv(socks5.messages.Reply)
        logging.info("server replied: %s", reply.reply_code)

        if reply.reply_code == socks5.constants.Replies.SUCCEEDED:
            sock.close()

        sink.close()

    client.close()


def timeout_test():
    logging.info("Running invalid destination test...")
    do_connect(socks5.constants.AddressTypes.IP_V4, ipaddress.IPv4Address('10.0.0.0'), 81)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s %(levelname)s:%(message)s")

    ipv6_test()
    invalid_conn_arg_test()
    invalid_dst_test()
    invalid_port_test()
    closed_port_test()
    client_drops_before_dest_test()
    timeout_test()
