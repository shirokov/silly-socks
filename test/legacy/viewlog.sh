#!/bin/sh

FULL_LOG=`mktemp /tmp/silly-socks-viewlog.XXXXXX`

for l in `ls *.log`
do
    echo "---=== $l ===---" >> $FULL_LOG
    cat $l >> $FULL_LOG
done

view $FULL_LOG
rm $FULL_LOG
