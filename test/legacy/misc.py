#!/usr/bin/env python3

# Copyright (c) 2018-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

''' Miscellaneous stuff for tests. '''

import logging
import os
import sys

sys.path.append("../src")

import socks5.messages
import socks5.constants

from client import Client


def random_payload(size):
    with open('/dev/urandom', 'rb') as f:
        return f.read(size)


def connect_client():
    client = Client(os.environ['TEST_HOST'], int(os.environ['TEST_PORT']))
    logging.debug("Connection OK")

    client.send(socks5.messages.VersionAndMethods([socks5.constants.Methods.NO_AUTH]))
    reply = client.recv(socks5.messages.MethodSelection)

    if reply.method == socks5.constants.Methods.NO_ACCEPTABLE_METHOD:
        logging.error("Server declined our methods")
        return None

    logging.info("Server has selected method %d", reply.method)
    if reply.method != socks5.constants.Methods.NO_AUTH:
        raise RuntimeError(f"not supported auth method: {reply.method}")

    return client
