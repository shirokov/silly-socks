#!/usr/bin/env python3

# Copyright (c) 2018-2019 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

import logging
import socket
import sys

sys.path.append("../src")

import socks5.parser

''' Client for Silly SOCKS tests. '''

class Client:
    def __init__(self, host, port, buf = 4096):
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.connect((host, port))
        self._buf = buf
        self._parser = socks5.parser.Parser()

    def recv_data(self, buf):
        data = self._sock.recv(buf)
        logging.debug("received: %s", data)
        return data

    def send(self, message):
        self.send_data(message.data())

    def send_data(self, data):
        logging.debug("sending: %s", data)
        self._sock.sendall(data)

    def recv(self, message_type):
        reply = None
        while not reply:
            data = self._sock.recv(self._buf)
            if len(data) == 0:
                logging.critical("server terminated the connection before sending %s", message_type)
                raise RuntimeError("connection closed")

            self._parser.feed(data)
            reply = self._parser.get(message_type)

        return reply

    def close(self):
        self._sock.close()
