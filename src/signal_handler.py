#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Signal handler catches SIGTERM and communicates it to the main loop via exception. """

import os
import selectors
import signal

from context_base import ContextBase


class KilledBySignal(Exception):
    pass


class SignalHandler(ContextBase):
    """ Mimics a socket context. """

    def __init__(self, sel):
        super().__init__()
        self.sel = sel
        self._read_end, self._write_end = os.pipe()

    def _name(self):
        return "SIGNAL HANDLER"

    def _callback(self, signum, stack_frame):
        os.write(self._write_end, bytes(1))

    def start(self):
        self.sel.register(self._read_end, selectors.EVENT_READ, self)
        signal.signal(signal.SIGTERM, self._callback)

    def close(self):
        self.sel.unregister(self._read_end)
        os.close(self._read_end)
        os.close(self._write_end)

    def read(self):
        raise KilledBySignal()

    def write(self):
        raise RuntimeError("signal handler can't write")

    def is_alive(self) -> bool:
        return True
