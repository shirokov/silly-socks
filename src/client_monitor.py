#!/usr/bin/env python3

# Copyright (c) 2018-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Class for monitoring client activities. """

import logging

from config import Config


class ClientMonitor:
    def __init__(self, config: Config, **kwargs):
        super().__init__(**kwargs)
        self._banned_addresses = set()
        self._failed_attempts = {}  # src_addr -> count
        self._config = config

    def is_addr_banned(self, addr):
        return addr[0] in self._banned_addresses

    def report_session_status_for_address(self, src_addr, successfull):
        # disregard the source port
        src_addr = src_addr[0]
        if successfull:
            logging.debug(
                "resetting failed attempts count for source address %s", src_addr
            )
            self._failed_attempts[src_addr] = 0
        else:
            failed_attempts = self._failed_attempts.get(src_addr, 0) + 1
            if failed_attempts >= self._config.max_session_attempts:
                logging.warning(
                    "ban source address %s due to max session attempts reached",
                    src_addr,
                )
                self._banned_addresses.add(src_addr)
            else:
                logging.debug(
                    "failed attempts count for source address %s is now %d",
                    src_addr,
                    failed_attempts,
                )
                self._failed_attempts[src_addr] = failed_attempts
