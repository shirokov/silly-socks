#!/usr/bin/env python3

# Copyright (c) 2019 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Context for CONNECT relays. """

import logging
import socket

from ready_status import ReadyStatus
from relay_context import RelayContext
from socket_context import SocketContext


class ConnectContext(RelayContext):
    @staticmethod
    def create_socket():
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setblocking(0)
        return sock

    def __init__(self, source_ctx, addr, timeout):
        super().__init__(self.create_socket(), source_ctx, timeout)
        self._source_ctx = source_ctx
        self._addr = addr

    def start(self):
        try:
            self._sock.connect(self._addr)
        except BlockingIOError:
            sock_err = self.get_socket_error()
            if not sock_err:
                logging.debug("%s: connection in progress", self.name())
                self._switch_to_mode(SocketContext.Mode.WRITE)
            else:
                self.status = ReadyStatus.from_errno(sock_err)
                self.close()
        except socket.gaierror as e:
            self.status = ReadyStatus.from_gai_error(e.errno)
            self.close()
        except socket.error as e:
            self.status = ReadyStatus.from_errno(e.errno)
            self.close()
        else:
            self._on_connected()

    def close(self):
        sock_err = self.get_socket_error()
        if sock_err:
            self.status = ReadyStatus.from_errno(sock_err)
        super().close()

    def _name(self):
        return f"CONNECT[{self._addr[0]}:{self._addr[1]}] <- {self._source_ctx.name()}"

    def on_data(self, data):
        self.transfer(data, self._source_ctx)

    # override PipeContext's write() in order to finalize connect
    def write(self):
        if self.status is ReadyStatus.SUCCESS:
            super().write()
        else:
            sock_err = self.get_socket_error()
            if not sock_err:
                self._on_connected()
            else:
                logging.debug("%s: failed to connect", self.name())
                self.status = ReadyStatus.from_errno(sock_err)
                self.close()

    def on_timeout(self):
        if self.status is not ReadyStatus.SUCCESS:
            self.status = ReadyStatus.TIMEOUT
        super().on_timeout()

    def _on_connected(self):
        logging.info("%s: connection established", self.name())
        self._switch_to_mode(SocketContext.Mode.READ)
        self.status = ReadyStatus.SUCCESS
        self._source_ctx._on_relay_ready()
