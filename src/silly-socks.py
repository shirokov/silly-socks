#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Code to launch the server. """

import contextlib
import daemon
import daemon.pidfile
import lockfile
import logging
import os
import platform
import sys

from args_config import config_from_commandline
from server import Server
from setup_logging import get_logging_files, setup_logging
from version import __version__


def log_greetings(version, pid):
    logging.info("starting Silly SOCKS v%s [PID=%d]", version, pid)
    logging.debug("running on %s %s", platform.system(), platform.release())
    logging.debug("Python version %s", sys.version.replace("\n", ""))


def log_farewell():
    logging.info("Silly SOCKS stopped")


def create_running_context(cfg):
    if not cfg.daemon:
        return contextlib.nullcontext()

    pid_file = daemon.pidfile.PIDLockFile(cfg.pid_file, timeout=-1)
    if pid_file.is_locked():
        pid = pid_file.read_pid()
        if pid is None:
            logging.warning(f"overwriting PID file")
        else:
            try:
                os.kill(pid, 0)
                logging.critical(
                    f"PID file is already locked by process {pid}", file=sys.stderr
                )
                sys.exit(1)
            except ProcessLookupError:
                logging.warning(f"overwriting PID file left by process {pid}")
        pid_file.break_lock()
    return daemon.DaemonContext(pidfile=pid_file, files_preserve=get_logging_files())


def run(cfg):
    if not setup_logging(cfg):
        sys.exit(1)

    pid = os.getpid()
    log_greetings(__version__, pid)

    if cfg.debug_mode:
        logging.debug("starting with the following configuration:")
        for l in cfg.dump():
            logging.debug("\t%s", l)

    with create_running_context(cfg):
        run_pid = os.getpid()
        if run_pid != pid:
            logging.info(f"server is running with PID={run_pid}")

        try:
            Server(cfg).run()
        except Exception as e:
            if cfg.debug_mode:
                raise
            logging.critical("failed to start due to an error: %s", e)
            sys.exit(1)
        except:
            if cfg.debug_mode:
                raise
            logging.critical("failed to start due to an unknown error")
            sys.exit(1)
        else:
            log_farewell()


if __name__ == "__main__":
    cfg = config_from_commandline()
    if not cfg or not cfg.validate():
        sys.exit(1)
    run(cfg)
