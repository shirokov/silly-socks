#!/usr/bin/env python3

# Copyright (c) 2018-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Context for client connections. """

import ipaddress
import logging

from connect_context import ConnectContext
from client_session import ClientSession
from pipe_context import PipeContext
from ready_status import ReadyStatus
from socket_context import SocketContext
from socks5.messages import Reply
from socks5.constants import *


class ClientContext(PipeContext):
    def __init__(self, sock, addr, parent):
        super().__init__(sock, parent)
        self._addr = addr
        self._session = ClientSession(self)
        self._relay = None
        self._server = parent._server
        self._timer = (
            self._server.new_timer(
                self._server.config.session_timeout, self._on_session_timeout
            )
            if self._server.config.session_timeout > 0
            else None
        )
        # FIXME: this flag is needed only because detection context close status is convoluted
        #        also the attempts count is reset only on (successfull) session close, better
        #        to report status after session close
        self._successful_attempt = False

    def _name(self):
        return f"CLIENT[{self._addr[0]}:{self._addr[1]}]"

    def on_data(self, data):
        if self._session is None:
            if self._relay:
                self.transfer(data, self._relay)
            else:
                logging.error(
                    "%s: attempt to transfer data without valid relay", self.name()
                )
                self.close()
        else:
            if not self._session.on_data(data):
                session_leftover = self._session.buffered()
                self._session = None
                if self._timer is not None:
                    self._timer.disarm()

                if self._relay is None:
                    logging.debug(
                        "%s: no relay has been established, closing", self.name()
                    )
                    self.close()

                if session_leftover > 0:
                    # RFC 1928 states that client may start transferring data only
                    # after getting a reply that indicates success, thus we treat
                    # any unparsed data left in the session as a violation of protocol
                    logging.error("%s: closed session still contains buffered data")
                    self.close()

    def start(self):
        self._switch_to_mode(SocketContext.Mode.READ)

    def close(self):
        if self._session is not None:
            self._session.close()
        # the relay, if any, will be closed by SocketContext,
        # and by setting relay to None we make sure
        # that no double close will happen in _on_child_quit
        self._relay = None
        if self._timer is not None:
            self._timer.disarm()
        super().close()
        if self._server.config.max_session_attempts > 0:
            self._server.report_session_status_for_address(
                self._addr, self._successful_attempt
            )

    def setup_connect_relay(self, addr, timeout):
        self._relay = ConnectContext(self, addr, timeout)
        self._add_child(self._relay)
        self._switch_to_mode(SocketContext.Mode.IDLE)
        self._successful_attempt = True

    def _send_reply(self, reply_code, finalizer=None):
        # FIXME: finalizer not used -- client session should not quit as long as the finalizer not executed
        if self._relay:
            addr, port = self._relay._sock.getsockname()
            # FIXME: set address type according to the relay's socket
            addr = ipaddress.IPv4Address(addr)
        else:
            # RFC 1928 doesn't specify which address the reply contains
            # when a failure is being reported, so we just put the server address
            addr, port = self._addr
            # FIXME: set address type according to the relay's socket
            addr = ipaddress.IPv4Address(addr)
        self.send(Reply(reply_code, AddressTypes.IP_V4, addr, port).data())

    def _on_relay_ready(self):
        self._set_read_mode(True)
        self._send_reply(Replies.SUCCEEDED)

    def _on_child_quit(self, ctx):
        if self._relay is not None:
            logging.debug("%s: relay quit", self.name())
            super()._on_child_quit(ctx)

            self._relay, status = None, self._relay.status
            if status is None or status is ReadyStatus.SUCCESS:
                # close the context right away
                self.close()
            else:
                # we need to notify the client that the request has failed
                self._send_reply(status.to_socks5_reply(), self.close)
                logging.error("%s: relay error: %s", self.name(), status)

    def _on_session_timeout(self):
        logging.info("%s: session timed out", self.name())
        self.close()
