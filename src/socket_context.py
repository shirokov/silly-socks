#!/usr/bin/env python3

# Copyright (c) 2018-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Base class for socket contexts. """

import enum
import logging
import selectors
import socket

from context_base import ContextBase
from server import Server


class SocketContext(ContextBase):
    class Mode(enum.Flag):
        IDLE = 0
        READ = enum.auto()
        WRITE = enum.auto()

    def _getsockopt(self, opt):
        return self._sock.getsockopt(socket.SOL_SOCKET, opt)

    def __init__(self, sock, server: Server, parent=None):
        super().__init__()
        self._sock = sock
        self._server = server
        self.recv_buf_size = self._getsockopt(socket.SO_RCVBUF)
        self.send_buf_size = self._getsockopt(socket.SO_SNDBUF)
        self._mode = self.Mode.IDLE
        self._parent = parent
        self._children = set()

    def is_alive(self) -> bool:
        return self._sock.fileno() >= 0

    def close(self):
        # FIXME: all this parent-child stuff is ridiculous!
        # iterate a copy because children will call _on_child_quit(),
        # and _children size will change triggering
        # a "...changed size during iteration..." RuntimeError exception
        for ctx in set(self._children):
            ctx.close()
        if self._mode is not self.Mode.IDLE:
            logging.debug(
                "%s: closing, unregister socket [fd=%d]",
                self.name(),
                self._sock.fileno(),
            )
            self._server.sel.unregister(self._sock)
        self._sock.close()
        if self._parent:
            self._parent._on_child_quit(self)
        logging.info("%s: closed", self.name())

    def get_socket_error(self):
        return self._getsockopt(socket.SO_ERROR)

    def _add_child(self, ctx):
        self._children.add(ctx)
        ctx.start()

    def _on_child_quit(self, ctx):
        self._children.remove(ctx)

    def _switch_to_mode(self, mode):
        if self._mode == mode:
            logging.debug("%s: won't switch to the same mode (%s)", self.name(), mode)
            return

        if self._mode is self.Mode.IDLE:
            logging.debug(
                "%s: register socket [fd=%d] for mode %s",
                self.name(),
                self._sock.fileno(),
                mode,
            )
            self._server.sel.register(self._sock, self._translate_mode(mode), self)
        elif mode is self.Mode.IDLE:
            logging.debug(
                "%s: going idle, unregister socket [fd=%d]",
                self.name(),
                self._sock.fileno(),
            )
            self._server.sel.unregister(self._sock)
        else:
            logging.debug(
                "%s: modify socket [fd=%d] for mode %s",
                self.name(),
                self._sock.fileno(),
                mode,
            )
            self._server.sel.modify(self._sock, self._translate_mode(mode), self)

        self._mode = mode

    @staticmethod
    def _translate_mode(mode):
        if mode is SocketContext.Mode.IDLE:
            raise RuntimeError("can't translate IDLE mode to a selector event")
        elif mode is SocketContext.Mode.READ:
            return selectors.EVENT_READ
        elif mode is SocketContext.Mode.WRITE:
            return selectors.EVENT_WRITE
        else:
            return selectors.EVENT_READ | selectors.EVENT_WRITE

    def _do_set_mode(self, mode, flag: bool):
        if flag == bool(self._mode & mode):
            return
        self._switch_to_mode(self._mode | mode if flag else self._mode & ~mode)

    def _set_read_mode(self, flag: bool):
        self._do_set_mode(SocketContext.Mode.READ, flag)

    def _set_write_mode(self, flag: bool):
        self._do_set_mode(SocketContext.Mode.WRITE, flag)
