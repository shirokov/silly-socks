#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" RFC 1928: version identifier/method selection message. """

from .constants import *


class VersionAndMethods:
    def __init__(self, methods):
        if len(methods) >= Methods.NO_ACCEPTABLE_METHOD:
            raise ValueError("too many methods")
        self.methods = methods

    def data(self):
        return bytes([PROTOCOL_VERSION, len(self.methods)] + self.methods)

    @staticmethod
    def builder(get_bytes):
        while True:
            try:
                data = None
                while not data:
                    data = get_bytes(2)
                    if not data:
                        yield None

                if data[0] != PROTOCOL_VERSION:
                    raise RuntimeError(
                        f"unsupported protocol version in VersionMethods: {data[0]}"
                    )

                n_methods = data[1]
                methods = []

                if n_methods > 0:
                    data = None
                    while not data:
                        data = get_bytes(n_methods)
                        if not data:
                            yield None
                    methods = list(data)

                yield VersionAndMethods(methods)
            except GeneratorExit:
                break
