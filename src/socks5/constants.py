#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" SOCKS5 constants from the RFCs. """

import enum


PROTOCOL_VERSION = 5
RESERVED_VALUE = 0x00


class Methods(enum.IntEnum):
    NO_AUTH = 0x00
    GSS_API = 0x01  # not supported!
    USERNAME_PASSWORD = 0x02
    # 0x03-0x7F - IANA ASSIGNED - not supported!
    # 0x80-0xFE - RESERVED FOR PRIVATE METHODS - no private methods
    NO_ACCEPTABLE_METHOD = 0xFF


class Commands(enum.IntEnum):
    CONNECT = 0x01
    BIND = 0x02
    UDP_ASSOCIATE = 0x3


class Replies(enum.IntEnum):
    SUCCEEDED = 0x00
    GENERAL_FAILURE = 0x01
    NOT_ALLOWED = 0x02
    NETWORK_UNREACHABLE = 0x03
    HOST_UNREACHABLE = 0x04
    CONNECTION_REFUSED = 0x05
    TTL_EXPIRED = 0x06
    COMMAND_NOT_SUPPORTED = 0x07
    ADDRESS_NOT_SUPPORTED = 0x08
    NOT_ASSIGNED = 0x09


class AddressTypes(enum.IntEnum):
    IP_V4 = 0x01
    DOMAINNAME = 0x03
    IP_V6 = 0x04


# constants below are from RFC 1929
USERNAME_PASSWORD_VERSION = 0x01


class AuthenticationStatuses(enum.IntEnum):
    SUCCESS = 0x00
    FAILURE = 0x01
