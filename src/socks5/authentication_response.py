#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" RFC 1929: a response message. """

from .constants import *


class AuthenticationResponse:
    def __init__(self, status):
        self.status = status

    def data(self):
        return bytes([USERNAME_PASSWORD_VERSION, self.status])

    @staticmethod
    def builder(get_bytes):
        while True:
            try:
                data = None
                while not data:
                    data = get_bytes(2)
                    if not data:
                        yield None

                if data[0] != USERNAME_PASSWORD_VERSION:
                    raise RuntimeError(
                        f"unsupported protocol version in AuthenticationResponse: {data[0]}"
                    )

                yield AuthenticationResponse(data[1])
            except GeneratorExit:
                break
