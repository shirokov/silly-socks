#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Stream parser for SOCKS 5 (RFC 1928). """

from .messages import MethodSelection, VersionAndMethods


class Parser:
    class _MessageInProgress:
        def __init__(self, message_type, builder):
            self.message_type = message_type
            self.builder = builder

    def __init__(self):
        self._stub = []
        self._idx = 0
        self._message = None

    def close(self):
        self._stub.clear()
        if self._message:
            self._message.builder.close()
            self._message = None

    def feed(self, data):
        self._stub.append(data)

    def stub_size(self):
        return sum([len(chunk) for chunk in self._stub])

    def get(self, message_type):
        if self._message:
            if self._message.message_type is not message_type:
                raise RuntimeError("already building another message type")

            result = self._message.builder.send(None)
            if result is not None:
                self._message.builder.close()
                self._message = None
            return result
        else:
            builder = message_type.builder(self._get_bytes)
            result = builder.send(None)
            if result is None:
                self._message = self._MessageInProgress(message_type, builder)
            return result

    def _get_bytes(self, n):
        result = bytes()
        while self._stub and len(result) < n:
            have = len(self._stub[0]) - self._idx
            if have == 0:
                self._stub.pop(0)
                self._idx = 0
                continue

            need = n - len(result)
            if have < need:
                result += bytes(self._stub[0])
                self._stub.pop(0)
                self._idx = 0
            else:
                new_idx = self._idx + need
                result += bytes(self._stub[0][self._idx : new_idx])
                if new_idx == len(self._stub[0]):
                    self._stub.pop(0)
                    self._idx = 0
                else:
                    self._idx = new_idx

        if len(result) < n:
            if len(result) > 0:
                self._stub.insert(0, result)
            return None

        return result
