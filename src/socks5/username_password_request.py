#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" RFC 1929: a Username/Password request message. """

from .constants import *


class UsernamePasswordRequest:
    def __init__(self, uname, passwd):
        if len(uname) > 0xFF:  # must fit in one byte
            raise ValueError("username is too long")

        if len(passwd) > 0xFF:  # must fit in one byte
            raise ValueError("password is too long")

        self.uname = uname
        self.passwd = passwd

    def data(self):
        return (
            bytes([USERNAME_PASSWORD_VERSION, len(self.uname)])
            + self.uname.encode("ascii")
            + bytes([len(self.passwd)])
            + self.passwd.encode("ascii")
        )

    @staticmethod
    def builder(get_bytes):
        while True:
            try:
                data = None
                while not data:
                    data = get_bytes(2)
                    if not data:
                        yield None

                if data[0] != USERNAME_PASSWORD_VERSION:
                    raise RuntimeError(
                        f"unsupported protocol version in UsernamePasswordRequest: {data[0]}"
                    )

                uname_len = int(data[1])
                if uname_len > 0:
                    data = None
                    while not data:
                        data = get_bytes(uname_len)
                        if not data:
                            yield None
                    uname = data.decode("ascii")
                else:
                    uname = ""

                data = None
                while not data:
                    data = get_bytes(1)
                    if not data:
                        yield None

                passwd_len = int(data[0])
                if passwd_len > 0:
                    data = None
                    while not data:
                        data = get_bytes(passwd_len)
                        if not data:
                            yield None
                    passwd = data.decode("ascii")
                else:
                    passwd = ""

                yield UsernamePasswordRequest(uname, passwd)
            except GeneratorExit:
                break
