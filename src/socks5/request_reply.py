#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" RFC 1928: request and reply messages. """

import ipaddress
import struct

from .constants import *


class _RequestReply:
    def __init__(self, address_type, address, port):
        self.address_type = address_type
        self.address = address
        self.port = port

    def get_addr(self):
        return (str(self.address), self.port)

    @staticmethod
    def _addr_from_bytes(address_type, data):
        if address_type == AddressTypes.IP_V4:
            return ipaddress.IPv4Address(data)
        elif address_type == AddressTypes.IP_V6:
            return ipaddress.IPv6Address(data)
        elif address_type == AddressTypes.DOMAINNAME:
            return data.decode("ascii")
        else:
            raise NotImplementedError(
                f"can't convert bytes to address type {address_type}"
            )

    @staticmethod
    def _addr2bytes(address_type, address):
        if address_type == AddressTypes.IP_V4 or address_type == AddressTypes.IP_V6:
            return address.packed
        elif address_type == AddressTypes.DOMAINNAME:
            return bytes([len(address)]) + address.encode("ascii")
        else:
            raise NotImplementedError(
                f"can't convert address type {address_type} to bytes"
            )

    @staticmethod
    def _port2bytes(port):
        return struct.pack("!H", port)

    @staticmethod
    def builder(get_bytes, second_field_type, message_type):
        while True:
            try:
                data = None
                while not data:
                    data = get_bytes(4)
                    if not data:
                        yield None

                if data[0] != PROTOCOL_VERSION:
                    raise RuntimeError(
                        f"unsupported protocol version in Request/Reply: {data[0]}"
                    )
                second_field = second_field_type(data[1])
                if data[2] != RESERVED_VALUE:
                    raise RuntimeError(f"wrong reserved value: {data[2]}")
                address_type = AddressTypes(data[3])

                if address_type == AddressTypes.IP_V4:
                    address_size = 4
                elif address_type == AddressTypes.DOMAINNAME:
                    data = None
                    while not data:
                        data = get_bytes(1)
                        if not data:
                            yield None
                    address_size = data[0]
                elif address_type == AddressTypes.IP_V6:
                    address_size = 16
                else:
                    raise NotImplementedError(
                        f"unsupported address type: {address_type}"
                    )

                data = None
                while not data:
                    data = get_bytes(address_size)
                    if not data:
                        yield None

                address = Request._addr_from_bytes(address_type, data)

                data = None
                while not data:
                    data = get_bytes(2)
                    if not data:
                        yield None

                port = struct.unpack("!H", data)[0]

                yield message_type(second_field, address_type, address, port)
            except GeneratorExit:
                break


class Request(_RequestReply):
    def __init__(
        self,
        command,
        address_type,
        address,
        port,
        check_command=True,
        check_address_type=True,
    ):
        if check_command and type(command) is not Commands:
            raise ValueError("invalid command provided")
        self.command = command
        if check_address_type and type(address_type) is not AddressTypes:
            raise ValueError("invalid address type provided")
        self._raw_address = not check_address_type
        super().__init__(address_type, address, port)

    def _addr2bytes(self):
        if self._raw_address:
            return self.address
        else:
            return _RequestReply._addr2bytes(self.address_type, self.address)

    def data(self):
        head = bytes(
            [PROTOCOL_VERSION, self.command, RESERVED_VALUE, self.address_type]
        )
        return head + self._addr2bytes() + self._port2bytes(self.port)

    builder = lambda get_bytes: _RequestReply.builder(get_bytes, Commands, Request)


class Reply(_RequestReply):
    def __init__(
        self,
        reply_code,
        address_type,
        address,
        port,
        check_reply_code=True,
        check_address_type=True,
    ):
        if check_reply_code and type(reply_code) is not Replies:
            raise ValueError("invalid reply code provided")
        self.reply_code = reply_code
        if check_address_type and type(address_type) is not AddressTypes:
            raise ValueError("invalid address type provided")
        super().__init__(address_type, address, port)

    def data(self):
        head = bytes(
            [PROTOCOL_VERSION, self.reply_code, RESERVED_VALUE, self.address_type]
        )
        return (
            head
            + self._addr2bytes(self.address_type, self.address)
            + self._port2bytes(self.port)
        )

    builder = lambda get_bytes: _RequestReply.builder(get_bytes, Replies, Reply)
