#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" RFC 1928: A METHOD selection message. """

from .constants import *


class MethodSelection:
    def __init__(self, method):
        self.method = method

    def data(self):
        return bytes([PROTOCOL_VERSION, self.method])

    @staticmethod
    def builder(get_bytes):
        while True:
            try:
                data = None
                while not data:
                    data = get_bytes(2)
                    if not data:
                        yield None

                if data[0] != PROTOCOL_VERSION:
                    raise RuntimeError(
                        f"unsupported protocol version in MethodSelection: {data[0]}"
                    )

                yield MethodSelection(data[1])
            except GeneratorExit:
                break
