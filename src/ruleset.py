#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Ruleset is a group of security-related configuration parameters. """

import enum
import ipaddress
import logging


def make_address_list(raw):
    result = []

    for addr in raw:
        try:
            result.append(ipaddress.ip_address(addr))
        except ValueError:
            result.append(addr)

    return result


class Ruleset:
    class _Modes(enum.Enum):
        UNRESTRICTED = enum.auto()
        BLACK_LIST = enum.auto()
        WHITE_LIST = enum.auto()

        def __str__(self):
            if self is self.UNRESTRICTED:
                return "unrestricted"
            elif self is self.BLACK_LIST:
                return "black list"
            elif self is self.WHITE_LIST:
                return "white list"
            else:
                raise ValueError("no description for ruleset mode")

    @staticmethod
    def _get_list_and_mode(dictionary):
        black_list = dictionary.get("black-list", None)
        white_list = dictionary.get("white-list", None)
        if black_list and white_list:
            raise RuntimeError("both black and white lists are specified")
        elif black_list:
            return make_address_list(black_list), Ruleset._Modes.BLACK_LIST
        elif white_list:
            return make_address_list(white_list), Ruleset._Modes.WHITE_LIST
        else:
            return None, Ruleset._Modes.UNRESTRICTED

    def __init__(self, name, dictionary={}):
        from yaml_config import parse_time

        self.name = name
        relay_timeout = dictionary.get("relay-timeout", None)
        self.relay_timeout = (
            parse_time(relay_timeout) if relay_timeout is not None else 0
        )
        self.connect_enabled = dictionary.get("connect-enabled", True)
        self._addr_list, self._mode = Ruleset._get_list_and_mode(dictionary)

    def dump(self, lines):
        lines.append(f"\trelay_timeout={self.relay_timeout}ms")  # TODO: nice time!
        lines.append(f"\tconnect_enabled={self.connect_enabled}")
        if self._mode is not self._Modes.UNRESTRICTED:
            lines.append(f"\t{self._mode}:")
            for addr in self._addr_list:
                lines.append(f"\t\t{addr}")

    def is_connect_allowed(self, addr, port) -> bool:
        logging.debug(
            "checking if connect to %s:%d is allowed in '%s'", addr, port, self.name
        )
        if self._mode is self._Modes.UNRESTRICTED:
            return True
        contains = addr in self._addr_list
        return not contains if self._mode is self._Modes.BLACK_LIST else contains
