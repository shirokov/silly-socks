#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Class for the configuration object. """

import sys

from ruleset import Ruleset


class Config:
    def __init__(self):
        """ Produces a default configuration. """
        self.host = None
        self.port = None
        self.users = {}  # username -> password
        self.no_auth = False
        self.listener_backlog = 32
        self.max_clients = 0  # unlimited
        self.transfer_buffer = 0  # unlimited
        self.session_timeout = 0  # no timeout
        self.max_session_attempts = 0  # unlimited
        self.daemon = False
        self.pid_file = "/var/run/silly-socks.pid"
        self.verbosity = 0
        self.debug_mode = False
        self.log_file = None
        self.default_ruleset = Ruleset("default")
        self.rulesets = {}  # name -> ruleset
        self.rules = {}  # usename -> ruleset name

    def dump(self):
        """ Returs a list of parameters string representations. """
        result = []
        result.append(f"host={self.host}")
        result.append(f"port={self.port}")
        for name, password in self.users.items():
            result.append(f"user {name}:{password}")
        result.append(f"no_auth={self.no_auth}")
        result.append(f"listener_baklog={self.listener_backlog}")
        result.append(f"max_clients={self.max_clients}")
        result.append(f"transfer_buffer={self.transfer_buffer}")
        result.append(f"session_timeout={self.session_timeout}ms")  # TODO: nice time!
        result.append(f"max_session_attemps={self.max_session_attempts}")
        result.append(f"daemon={self.daemon}")
        result.append(f"pid_file={self.pid_file}")
        result.append(f"verbosity={self.verbosity}")
        result.append(f"debug_mode={self.debug_mode}")
        result.append(
            f"log_file={self.log_file if self.log_file is not None else '<stdout>'}"
        )
        result.append("default ruleset:")
        self.default_ruleset.dump(result)
        for n, r in self.rulesets.items():
            result.append(f"ruleset '{n}':")
            r.dump(result)
        if self.rules:
            result.append("rules:")
            for u, r in self.rules.items():
                result.append(f"\t{u} -> ruleset '{r.name}'")
        return result

    def validate(self) -> bool:
        if self.host is None:
            print("host not configured", file=sys.stderr)
            return False

        if self.port is None:
            print("port not configured", file=sys.stderr)
            return False

        if self.daemon and not self.log_file:
            print("can't run as a daemon without log file configured", file=sys.stderr)
            return False

        return True
