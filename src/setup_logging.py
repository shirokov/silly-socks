#!/usr/bin/env python3

# Copyright (c) 2020-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Code to setup logging. """

import logging
import sys

from config import Config


class LoggerAdaptor:
    def __init__(self, name, log_level, original):
        self._prefix = f"<{name}>"
        self._logger = logging.getLogger(name)
        self._log_level = log_level
        self._original = original

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self._logger.log(self._log_level, "%s %s", self._prefix, line.rstrip())

    def flush(self):
        pass

    # DaemonContext will close the original file
    def fileno(self):
        return self._original.fileno()


def get_basic_params(cfg: Config):
    if cfg.debug_mode:
        return logging.DEBUG, "%(asctime)s %(levelname)s:%(module)s:%(message)s"
    elif cfg.verbosity >= 2:
        return logging.INFO, "%(asctime)s %(levelname)s:%(message)s"
    elif cfg.verbosity == 1:
        return logging.WARNING, "%(asctime)s %(levelname)s:%(message)s"
    else:
        return logging.ERROR, "%(asctime)s %(levelname)s:%(message)s"


def intercept_std_streams():
    sys.stdout = LoggerAdaptor("STDOUT", logging.INFO, sys.stdout)
    sys.stderr = LoggerAdaptor("STDERR", logging.ERROR, sys.stderr)


def get_files_from_logger(logger):
    result = []
    for handler in logger.handlers:
        if isinstance(handler, logging.FileHandler):
            logging.debug(f"will preserve file for logger {logger}, handler {handler}")
            result.append(handler.stream.fileno())
    return result


def get_logging_files():
    result = get_files_from_logger(logging.getLogger())
    for _, logger in logging.Logger.manager.loggerDict.items():
        result += get_files_from_logger(logger)
    return result


def setup_logging(cfg: Config) -> bool:
    try:
        log_level, log_format = get_basic_params(cfg)
        logging.basicConfig(level=log_level, format=log_format, filename=cfg.log_file)
        intercept_std_streams()
    except Exception as e:
        print(f"can't setup logging: {e}", file=sys.stderr)
        return False
    else:
        return True
