#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Base class for all relay contexts. """

import logging

from pipe_context import PipeContext


class RelayContext(PipeContext):
    def __init__(self, sock, source_ctx, timeout):
        super().__init__(sock, source_ctx)
        self.status = None  # sublasses shall assign a ReadyStatus instance
        self._timer = (
            self._server.new_timer(timeout, self._on_timeout) if timeout > 0 else None
        )
        if self._timer is not None:
            self._recharge = lambda: self._server.recharge_timer(self._timer, timeout)

    def close(self):
        if self._timer is not None:
            self._timer.disarm()
        super().close()

    def _on_timeout(self):
        logging.info("%s relay timed out", self.name())
        self.on_timeout()

    def on_timeout(self):
        self.close()

    def transfer(self, data, source):
        if self._timer is not None:
            self._timer = self._recharge()
        super().transfer(data, source)

    def write(self):
        if self._timer is not None:
            self._timer = self._recharge()
        super().write()
