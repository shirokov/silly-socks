#!/usr/bin/env python3

# Copyright (c) 2018-2022 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Class for monitoring client activities. """

import abc
import heapq


class Timer(abc.ABC):
    @abc.abstractmethod
    def disarm(self):
        pass


class TimerQueue:
    class _Timer(Timer):
        def __init__(self, milliseconds, callback):
            self.milliseconds = milliseconds
            self.callback = callback
            self.armed = True

        def __lt__(self, other):
            return self.milliseconds < other.milliseconds

        def disarm(self):
            self.armed = False

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._heap = []  # will mantain a min heap via heapq
        self._fresh_timers = (
            []
        )  # we keep fresh timers from expiration on the same cycle they were created
        self._locked = False  # use a simple flag to lock during the expiration loop

    def new_timer(self, milliseconds, callback) -> Timer:
        timer = TimerQueue._Timer(milliseconds, callback)
        if self._locked:
            self._fresh_timers.append(timer)
        else:
            assert not self._fresh_timers, "non-empty fresh timers list outside the expiry loop"
            heapq.heappush(self._heap, timer)
        return timer

    def recharge_timer(self, timer, milliseconds) -> Timer:
        timer.disarm()
        return self.new_timer(milliseconds, timer.callback)

    def nearest_timer_expiry(self) -> int:
        """ Returns nearest timeout. """
        assert not self._locked, "nearest timer expiry is unreliable during the expiry loop"
        # pop disarmed timers
        while self._heap and not self._heap[0].armed:
            heapq.heappop(self._heap)
        return self._heap[0].milliseconds if self._heap else None

    def expire_timers(self, milliseconds):
        assert not self._locked, "can't expire timers recursively"
        self._locked = True

        while self._heap:
            if not self._heap[0].armed:
                heapq.heappop(self._heap)
            elif self._heap[0].milliseconds <= milliseconds:
                self._heap[0].callback()
                heapq.heappop(self._heap)
            else:
                break

        for t in self._heap:
            t.milliseconds -= milliseconds

        for t in self._fresh_timers:
            heapq.heappush(self._heap, t)
        self._fresh_timers.clear()

        self._locked = False
