#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Server object that runs the event loop. """

import errno
import logging
import selectors
import socket
import sys
import time

from client_monitor import ClientMonitor
from config import Config
from signal_handler import KilledBySignal, SignalHandler
from timer_queue import TimerQueue


class Server(ClientMonitor, TimerQueue):
    def __init__(self, config: Config):
        super().__init__(config=config)
        self._check_config(config)
        self.config = config
        self.sel = selectors.DefaultSelector()

    def run(self):
        from listener_context import ListenerContext

        with SignalHandler(self.sel), ListenerContext(self):
            while True:
                try:
                    timestamp = time.monotonic()  # in fractional seconds
                    self._do_select(self.nearest_timer_expiry())
                    self.expire_timers(int((time.monotonic() - timestamp) * 1000))
                except KeyboardInterrupt:
                    logging.info("stopping due to keyboard interrupt")
                    break
                except KilledBySignal:
                    logging.info("stopping due to a signal")
                    break
                except Exception as e:
                    logging.critical("stopping due to error: %s", e)
                    if self.config.debug_mode:
                        raise
                    break
                except:
                    logging.critical("stopping due to an unknown error")
                    if self.config.debug_mode:
                        raise
                    break

    def _do_select(self, timeout) -> bool:
        if timeout is not None:
            timeout = self._timeout_to_seconds(timeout)
        if self._config.debug_mode:
            logging.debug(
                "select for %s", "infinity" if timeout is None else f"{timeout}s"
            )

        for key, events in self.sel.select(timeout):
            ctx = key.data
            if ctx.is_alive() and (events & selectors.EVENT_READ):
                logging.debug("%s is ready to read", ctx.name())
                ctx.read()
            if ctx.is_alive() and (events & selectors.EVENT_WRITE):
                logging.debug("%s is ready to write", ctx.name())
                ctx.write()

    @staticmethod
    def _timeout_to_seconds(timeout):
        return timeout / 1000

    @staticmethod
    def _check_config(config):
        if config.users:
            for login, password in config.users.items():
                if len(password) == 0:
                    logging.warning("user '%s' has an empty password", login)

            if config.no_auth:
                logging.warning("NO_AUTH enabled along with configured users")
        else:
            if not config.no_auth:
                logging.warning("no users are configured for authentication")
