#!/usr/bin/env python3

# Copyright (c) 2019-2023 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Silly SOCKS version. """

__version__ = "0.3.1"
