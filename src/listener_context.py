#!/usr/bin/env python3

# Copyright (c) 2018-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Context for the listening socket. """

import logging
import select
import socket

from client_context import ClientContext
from server import Server
from socket_context import SocketContext


class ListenerContext(SocketContext):
    @staticmethod
    def create_socket():
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.setblocking(0)
        return sock

    def __init__(self, server: Server):
        super().__init__(self.create_socket(), server)
        self._addr = (self._server.config.host, self._server.config.port)

    def _bind(self):
        try:
            self._sock.bind(self._addr)
        except OverflowError as e:
            raise RuntimeError(f"bad listener port: {self._addr[1]}") from e

    def start(self):
        self._bind()
        self._sock.listen(self._server.config.listener_backlog)
        self._switch_to_mode(SocketContext.Mode.READ)

    def _name(self):
        return f"LISTENER[{self._addr[0]}:{self._addr[1]}]"

    def read(self):
        sock, addr = self._sock.accept()
        if (
            self._server.config.max_clients > 0
            and len(self._children) > self._server.config.max_clients
        ):
            logging.warning(
                "%s: dropping a client[%s:%s] due to the limit reached",
                self.name(),
                addr[0],
                addr[1],
            )
            sock.close()
            return
        if self._server.is_addr_banned(addr):
            logging.warning(
                "%s: dropping a banned client[%s:%s]", self.name(), addr[0], addr[1]
            )
            sock.close()
            return

        sock.setblocking(0)

        logging.info(
            "%s: accepted connection from %s:%s", self.name(), addr[0], addr[1]
        )

        client = ClientContext(sock, addr, self)
        client.start()

        self._children.add(client)

    def write(self):
        raise RuntimeError("no write for listening context")
