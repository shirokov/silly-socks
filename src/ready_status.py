#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Relay status indicator. """

import errno
import os

from socks5.constants import Replies


class ReadyStatus:
    """ Use one of the class constants or an instance via one of the static methods. """

    class _Errno:
        def __init__(self, error_code):
            self.error_code = error_code

        def to_socks5_reply(self):
            if self.error_code == errno.ENETUNREACH:
                return Replies.NETWORK_UNREACHABLE
            elif self.error_code == errno.EHOSTUNREACH:
                return Replies.HOST_UNREACHABLE
            elif self.error_code == errno.ECONNREFUSED:
                return Replies.CONNECTION_REFUSED
            elif self.error_code == errno.EADDRNOTAVAIL:
                return Replies.HOST_UNREACHABLE
            elif self.error_code == errno.ETIMEDOUT:
                return Replies.TTL_EXPIRED
            else:
                return Replies.GENERAL_FAILURE

    class _GaiError:
        def __init__(self, error_code):
            self.error_code = error_code

        def to_socks5_reply(self):
            return Replies.HOST_UNREACHABLE

    def __init__(self, error, message):
        self._error = error
        self._message = message

    @staticmethod
    def from_errno(error_code, message=None):
        if message is None:
            message = ReadyStatus._strerr(error_code)
        return ReadyStatus(ReadyStatus._Errno(error_code), message)

    @staticmethod
    def from_gai_error(error_code):
        return ReadyStatus(ReadyStatus._GaiError(error_code), "getaddrinfo failed")

    @staticmethod
    def _strerr(err):
        return f"{errno.errorcode[err]}: {os.strerror(err)}" if err != 0 else None

    def to_socks5_reply(self):
        if self._error is None:
            raise RuntimeError("can't convert to a SOCKS5 reply")
        return self._error.to_socks5_reply()

    def __str__(self):
        if self._error is None or self._message is None:
            raise RuntimeError("can't convert to a string")
        return f"{self._error.error_code}: {self._message}"


ReadyStatus.SUCCESS = ReadyStatus(None, None)
ReadyStatus.TIMEOUT = ReadyStatus.from_errno(errno.ETIMEDOUT, "relay timeout")
