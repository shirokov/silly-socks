#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Read config from a YAML file. """

import yaml

from config import Config
from ruleset import Ruleset


def translate_verbosity(value):
    if value == "error":
        return 0
    elif value == "warning":
        return 1
    elif value == "info":
        return 2
    else:
        raise ValueError(f"bad verbosity value: {value}")


def parse_time(value):
    # TODO: scan literals like s, m, h, d
    return value


def parse(filename) -> Config:
    with open(filename, "r") as f:
        cfg = yaml.load(f, Loader=yaml.SafeLoader)
        if not isinstance(cfg, dict):
            raise RuntimeError("invalid configuration file")

    result = Config()

    server_cfg = cfg.get("server", None)
    if server_cfg:
        result.host = server_cfg.get("host", result.host)
        result.port = server_cfg.get("port", result.port)
        result.max_clients = server_cfg.get("max-clients", result.max_clients)
        result.transfer_buffer = server_cfg.get(
            "transfer-buffer", result.transfer_buffer
        )
        session_timeout = server_cfg.get("session-timeout", None)
        if session_timeout is not None:
            result.session_timeout = parse_time(session_timeout)
        result.max_session_attempts = server_cfg.get(
            "max-session-attempts", result.max_session_attempts
        )
        result.listener_backlog = server_cfg.get(
            "listener-backlog", result.listener_backlog
        )
        result.daemon = server_cfg.get("daemon", result.daemon)
        result.pid_file = server_cfg.get("pid-file", result.pid_file)
        verbosity = server_cfg.get("verbosity", None)
        if verbosity:
            result.verbosity = translate_verbosity(verbosity)
        result.log_file = server_cfg.get("log-file", result.log_file)
        result.debug_mode = server_cfg.get("debug-mode", result.debug_mode)

    auth_cfg = cfg.get("auth", None)
    if auth_cfg:
        users = auth_cfg.get("users", [])
        for u in users:
            if len(u) != 1:
                raise RuntimeError(f"failed to add user: {u}")
            for login, password in u.items():
                result.users[login] = password
        result.no_auth = auth_cfg.get("no-auth", result.no_auth)

    rulesets = cfg.get("rulesets", {})
    for name, dictionary in rulesets.items():
        ruleset = Ruleset(name, dictionary)
        if name == "default":
            result.default_ruleset = ruleset
        else:
            result.rulesets[name] = ruleset

    rules = cfg.get("rules", {})
    for username, ruleset_name in rules.items():
        if username not in result.users:
            raise RuntimeError(f"rule for unknown user '{username}'")
        if ruleset_name not in result.rulesets:
            raise RuntimeError(
                f"rule for user '{username}' refers to unknown ruleset '{ruleset_name}'"
            )
        result.rules[username] = result.rulesets[ruleset_name]

    return result
