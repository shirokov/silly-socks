#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Client session coroutine. """

import logging

from pipe_context import PipeContext
from socks5.constants import *
from socks5.messages import *
from socks5.parser import Parser


class ClientSession:
    def __init__(self, context):
        self._parser = Parser()
        self._context = context
        self._coro = self._coroutine(self._context, self)

    def buffered(self):
        return self._parser.stub_size()

    def close(self):
        self._parser.close()
        if self._coro:
            self._coro.close()
            self._coro = None

    def on_data(self, data) -> bool:
        """ Returns True if the session is alive, False otherwise. """
        if not self._coro:
            logging.debug(
                "%s: passing data to an inactive session", self._context.name()
            )
            return False

        try:
            self._parser.feed(data)
            self._coro.send(None)
        except StopIteration:
            logging.debug("%s: session quit", self._context.name())
            return False

        return True

    @staticmethod
    def _coroutine(context, session):
        # FIXME: sends from coroutine may not complete immediately,
        #        thus the current implementation proceeds without
        #        any confidence that the client got all the data
        #        currently that works, but the correct behaviour
        #        is to call send with a finalizer that will call
        #        the couroutine and yield
        logging.debug("%s: session started", context.name())

        try:
            message = None
            while message is None:
                message = session._parser.get(VersionAndMethods)
                if message is None:
                    yield

            logging.debug(
                "%s: client sent methods: %s",
                context.name(),
                " ".join([str(m) for m in message.methods]),
            )
            if not message.methods:
                logging.warning("client sent zero methods")

            rules = context._server.config.default_ruleset
            if context._server.config.no_auth and Methods.NO_AUTH in message.methods:
                logging.info("%s: proceeding without authentication", context.name())
                context.send(MethodSelection(Methods.NO_AUTH).data())
            elif (
                context._server.config.users
                and Methods.USERNAME_PASSWORD in message.methods
            ):
                logging.debug(
                    "%s: starting Username/Password authentication", context.name()
                )
                context.send(MethodSelection(Methods.USERNAME_PASSWORD).data())
                message = None

                while message is None:
                    message = session._parser.get(UsernamePasswordRequest)
                    if message is None:
                        yield

                if not message.uname:
                    logging.warning("%s: provides an empty user name", context.name())
                    context.send(
                        AuthenticationResponse(AuthenticationStatuses.FAILURE).data()
                    )
                    return
                elif not message.passwd:
                    logging.warning(
                        "%s: provides an empty password (username:'%s')",
                        context.name(),
                        message.uname,
                    )
                    context.send(
                        AuthenticationResponse(AuthenticationStatuses.FAILURE).data()
                    )
                elif message.uname not in context._server.config.users:
                    logging.warning(
                        "%s: provides an unknown user name: '%s'",
                        context.name(),
                        message.uname,
                    )
                    context.send(
                        AuthenticationResponse(AuthenticationStatuses.FAILURE).data()
                    )
                    return
                elif message.passwd != context._server.config.users[message.uname]:
                    logging.warning(
                        "%s: provides wrong password '%s' for user %s",
                        context.name(),
                        message.passwd,
                        message.uname,
                    )
                    context.send(
                        AuthenticationResponse(AuthenticationStatuses.FAILURE).data()
                    )
                    return

                context.send(
                    AuthenticationResponse(AuthenticationStatuses.SUCCESS).data()
                )
                logging.info(
                    "%s: authenticated as user %s", context.name(), message.uname
                )
                if message.uname in context._server.config.rules:
                    rules = context._server.config.rules[message.uname]
            else:
                logging.warning("%s: no acceptable method", context.name())
                context.send(MethodSelection(Methods.NO_ACCEPTABLE_METHOD).data())
                return

            message = None
            while message is None:
                message = session._parser.get(Request)
                if message is None:
                    yield

            ClientSession._process_request(context, message, rules)
        except GeneratorExit:
            logging.debug("%s: session closed", context.name())

    @staticmethod
    def _send_reply(context, reply_code, message):
        context.send(
            Reply(
                reply_code, message.address_type, message.address, message.port
            ).data()
        )

    @staticmethod
    def _process_request(context, request, rules):
        if request.address_type == AddressTypes.IP_V6:
            ClientSession._send_reply(context, Replies.ADDRESS_NOT_SUPPORTED, request)
            logging.info(
                "client request declined, as the address type is not supported"
            )
        elif request.command == Commands.CONNECT:
            logging.debug(
                "%s: connect to %s:%d requested",
                context.name(),
                str(request.address),
                request.port,
            )
            if not rules.connect_enabled:
                ClientSession._send_reply(context, Replies.NOT_ALLOWED, request)
                logging.info(
                    "client request declined, as the CONNECT command is disabled"
                )
            elif not rules.is_connect_allowed(request.address, request.port):
                ClientSession._send_reply(context, Replies.NOT_ALLOWED, request)
                logging.warning(
                    "client request declined: CONNECT to %s:%d is not allowed by ruleset",
                    request.address,
                    request.port,
                )
            else:
                logging.info(
                    "%s: connecting to %s:%d",
                    context.name(),
                    str(request.address),
                    request.port,
                )
                context.setup_connect_relay(request.get_addr(), rules.relay_timeout)
        elif request.command == Commands.BIND:
            logging.debug("bind to %s:%d requested", str(request.address), request.port)
            ClientSession._send_reply(context, Replies.COMMAND_NOT_SUPPORTED, request)
            logging.info(
                "client request declined, as the BIND command is not supported"
            )
        elif request.command == Commands.UDP_ASSOCIATE:
            logging.debug(
                "UDP associate with %s:%d requested", str(request.address), request.port
            )
            ClientSession._send_reply(context, Replies.COMMAND_NOT_SUPPORTED, request)
            logging.info(
                "client request declined, as the UDP ASSOCIATE command is not supported"
            )
