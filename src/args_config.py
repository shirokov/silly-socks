#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Read config from command line arguments. """

import argparse
import pathlib
import sys

from config import Config
from version import __version__
from yaml_config import parse as parse_yaml


def check_unsigned(value):
    i = int(value)
    if i < 0:
        raise argparse.ArgumentTypeError("value should be >= 0")
    return i


def login_password(value):
    pos = value.find(":")
    if pos < 0:
        raise argparse.ArgumentTypeError("can't find the colon in login:password")
    login, password = value[:pos], value[pos + 1 :]
    return (login, password)


def yaml_file(value):
    try:
        return parse_yaml(value)
    except Exception as e:
        raise argparse.ArgumentTypeError(
            f"failed to load configuration from '{value}': {e}"
        ) from None


def parse() -> Config:
    parser = argparse.ArgumentParser(
        description="Silly SOCKS proxy server", add_help=False
    )

    server_group = parser.add_argument_group("server", "server configuration")
    server_group.add_argument("host", default=None, nargs="?", help="server host")
    server_group.add_argument(
        "port", default=None, nargs="?", type=check_unsigned, help="server port"
    )
    server_group.add_argument(
        "-M",
        "--max-clients",
        help="maximum number of clients, 0 for unlimited",
        type=check_unsigned,
    )
    server_group.add_argument(
        "-B",
        "--transfer-buffer",
        help="transfer buffer size, 0 for unlimited",
        type=check_unsigned,
    )
    server_group.add_argument(
        "-T",
        "--session-timeout",
        help="session timeout in milliseconds, 0 for no timeout",
        type=check_unsigned,
    )
    server_group.add_argument(
        "-A",
        "--max-session-attempts",
        help="maximum number of unsuccessfull attempts to establish session, 0 for unlimited",
        type=check_unsigned,
    )
    server_group.add_argument(
        "--disable-connect", help="disable CONNECT command", action="store_true"
    )
    server_group.add_argument(
        "-D", "--daemon", help="run as a daemon", action="store_true"
    )
    server_group.add_argument("-P", "--pid-file", help="PID file location for daemon")
    server_group.add_argument(
        "-v", "--verbosity", help="increase output verbosity", action="count"
    )
    server_group.add_argument("-l", "--log-file", help="write log to this file")
    server_group.add_argument("-d", "--debug", help="debug mode", action="store_true")

    auth_group = parser.add_argument_group("auth", "authentication")
    auth_group.add_argument(
        "--no-auth", help="allow no authentication", action="store_true"
    )
    auth_group.add_argument(
        "-u",
        "--user",
        help="add user in format <login>:<password>",
        type=login_password,
        action="append",
    )

    other_group = parser.add_argument_group("other")
    other_group.add_argument(
        "-f",
        "--file",
        help="load configuration from YAML file",
        type=yaml_file,
        default=Config(),
    )
    other_group.add_argument(
        "--version", action="version", version=f"%(prog)s {__version__}"
    )
    other_group.add_argument(
        "-h", "--help", action="help", help="show this help message and exit"
    )

    args = parser.parse_args()

    result = args.file

    if args.host is not None:
        result.host = args.host
    if args.port is not None:
        result.port = args.port
    if args.max_clients is not None:
        result.max_clients = args.max_clients
    if args.transfer_buffer is not None:
        result.transfer_buffer = args.transfer_buffer
    if args.session_timeout is not None:
        result.session_timeout = args.session_timeout
    if args.max_session_attempts is not None:
        result.max_session_attempts = args.max_session_attempts
    if args.disable_connect:
        result.default_ruleset.connect_enabled = False
    if args.daemon:
        result.daemon = True
    if args.pid_file:
        result.pid_file = args.pid_file
    if args.verbosity is not None:
        result.verbosity = args.verbosity
    if args.debug:
        result.debug_mode = True
    if args.log_file:
        result.log_file = args.log_file

    if args.no_auth:
        result.no_auth = True
    if args.user:
        result.users.update(dict(args.user))

    return result


def config_from_commandline():
    if len(sys.argv) > 1:
        return parse()

    config_locations = [
        pathlib.Path.home() / ".silly-socks.yml",
        pathlib.Path("/etc/silly-socks.yml"),
        pathlib.Path("../conf/silly-socks.yml"),
    ]

    for guess in config_locations:
        if guess.exists():
            return yaml_file(guess)

    print("can't locate the config file", file=sys.stderr)
