#!/usr/bin/env python3

# Copyright (c) 2019 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Base class for contexts running on the server. """

import abc


class ContextBase(abc.ABC):
    def __init__(self):
        self._cached_name = None

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    @abc.abstractmethod
    def _name(self):
        pass

    def name(self):
        if self._cached_name is None:
            self._cached_name = self._name()
        return self._cached_name

    @abc.abstractmethod
    def start(self):
        pass

    @abc.abstractmethod
    def close(self):
        pass

    @abc.abstractmethod
    def read(self):
        pass

    @abc.abstractmethod
    def write(self):
        pass

    @abc.abstractmethod
    def is_alive(self) -> bool:
        pass
