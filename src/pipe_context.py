#!/usr/bin/env python3

# Copyright (c) 2019-2021 Konstantin Shirokov
# Licensed under the MIT License. See LICENSE for details.

""" Context that acts as a bidirectional pipe. """

import abc
import logging

from socket_context import SocketContext


class PipeContext(SocketContext):
    def __init__(self, sock, parent):
        super().__init__(sock, parent._server, parent)
        self._queue = []
        self._total = 0

    def read(self):
        try:
            to_read = self.recv_buf_size
            data = self._sock.recv(to_read)
            if len(data) > 0:
                self.on_data(data)
            else:
                logging.info("%s: connection closed from the other side", self.name())
                self.close()
        except Exception as e:
            logging.error("%s: read error: %s", self.name(), e)
            if self._server.config.debug_mode:
                logging.exception(e)
            self.close()

    def write(self):
        while self._total > 0:
            to_send = self._queue.pop(0)
            size = len(to_send[0])
            sent = self._do_send(to_send[0])

            if sent is None:
                return
            elif sent == 0:
                break

            self._total -= sent
            if sent < size:
                self._queue.insert(0, (to_send[0][sent:], to_send[1]))
                break
            else:
                if to_send[1]:
                    # call the finalizer
                    to_send[1]()

        if self._total == 0:
            self._set_write_mode(False)

    def _do_send(self, data):
        try:
            return self._sock.send(data)
        except BlockingIOError:
            return 0
        except Exception as e:
            logging.error("%s: write error: %s", self.name(), e)
            if self._server.config.debug_mode:
                logging.exception(e)
            self.close()
            return None

    def send(self, data, finalizer=None):
        if self._total > 0:
            self._add_to_queue(data, finalizer)
        else:
            to_send = len(data)
            sent = self._do_send(data)
            if sent is None:
                return
            elif sent == to_send:
                if finalizer:
                    finalizer()
            else:
                self._add_to_queue(data[sent:], finalizer)
                self._set_write_mode(True)

    def _transfer_cont(self, pipe):
        if pipe._total == 0:
            self._set_read_mode(True)

    def transfer(self, data, pipe):
        if not pipe._server.config.transfer_buffer:
            pipe.send(data)
            return

        if pipe._total == 0:
            sent = pipe._do_send(data)
            if sent == len(data) or sent is None:
                return
            data = data[sent:]

        pipe._add_to_queue(data, lambda: self._transfer_cont(pipe))
        if pipe._total >= pipe._server.config.transfer_buffer:
            self._set_read_mode(False)
        pipe._set_write_mode(True)

    @abc.abstractmethod
    def on_data(self, data):
        """ Passed data always has size > 0. """
        pass

    def _add_to_queue(self, data, finalizer):
        self._queue.append((data, finalizer))
        self._total += len(data)
