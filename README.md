# Silly SOCKS
A SOCKS5 server in Python 3.  At the moment the server implements only the CONNECT command.

## Installation

Examples assume a Debian-based system.

### Virtualenv

0. Install `virtualenv` (may need to run via `sudo`):
```
apt install python3-pip
pip3 install virtualenv
```

1. Create a virtual environment (called `venv` in this example):
```
cd <silly-socks directory>
virtualenv venv
```

2. Activate the virtual environment and install requirements:
```
source venv/bin/activate
pip3 install -r requirements.txt
```

3. Create a handy link (assuming `~/bin` in in your `$PATH`)
```
ln -s ~/bin/silly-socks <silly-socks directory>/src/silly-socks.py
```

### System - assumes root priveleges

0. Install requirements as system packages
```
apt install python3-daemon python3-yaml
```

1. Place Silly SOCKS into `/opt`
```
cp -r <silly-socks directory> /opt
```

2. Create a handy link (make sure `/opt/bin` is in your `$PATH`!)
```
ln -s /opt/silly-socks/src/silly-socks.py /opt/bin/silly-socks
```

## Getting Started

### Running the tests

0. Current target is `pytest` which comes along with other `pip3` requirements in the virtual environment.

1. It's just as easy as running  `pytest` from root directory of the project.
```
cd <silly-socks directory>/
pytest
```

### Running the server

The server can be started from command line:
```
sillly-socks --no-auth 0.0.0.0 1080
```
Note `--no-auth` flag that disables uresname-password authorization. Alternatively, starting with a user `client` with password `swordfish`:
```
silly-socks -u client:swordfish 0.0.0.0 1080
```
Configuration from an YAML file is also supported:
```
sillly-socks -f conf/sample.yml
```
Finally, in case started without arguments, the server will try to find the configuration file in the following locations:
```
~/.silly-socks.yml
/etc/silly-socks.yml
<project root>/conf/silly-socks.yml
```

## Notes
I wrote Silly SOCKS as an experiment - just to check how easy is it to write a proxy server.
Back then I needed one for proxying an instant messager for a small userbase of 3 people.
It has come a long way from just an SSH tunnel to a more-less usable Python product.

I just added stuff and experimented, in no way Silly SOCKS is a Production-ready system!
(would you run anything with "silly" in its name in your Production anyway?)
The next big thing on the roadmap is to move the server over Python native `async`, but I am not sure I'll ever get to that.
I am just releasing the stuff I've got on the `dev` branch as version `0.3.1` - and considering the project abandoned.

Have never tried running it on Windows. If you manage to do that, please create a pull request!

The server is not RFC 1928 compliant as there's no support for GSSAPI authentication!
IPv6 is not currently supported!

## References:
- Repository: https://gitlab.com/shirokov/silly-socks/
- RFC 1928: https://www.ietf.org/rfc/rfc1928.txt
- RFC 1929: https://www.ietf.org/rfc/rfc1929.txt
